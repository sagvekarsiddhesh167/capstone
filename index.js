const nav = document.querySelector('nav');
window.addEventListener('scroll', fixNav);

function fixNav() {
    if(window.scrollY > nav.offsetHeight + 150) {
        nav.classList.add('active');
    } else {
        nav.classList.remove('active');
    }
}





// 
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }
  
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  function alumini() {
    const offset = window.scrollY + document.querySelector(`#alumini`).getBoundingClientRect().top - 160;
    window.scrollTo({ top: offset, behavior: "smooth" });
  }